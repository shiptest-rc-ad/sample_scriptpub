#!/bin/bash -e

echo "hello from public script"

export TEST_RES_IMAGE_OUT="ub-image2invalidimg"
export TEST_CURR_JOB="ub-inrunSh" 

export TEST_RES_IMAGE_OUT_UP=$(echo $TEST_RES_IMAGE_OUT | awk '{print toupper($0)}')
export TEST_RES_IMAGE_OUT_VERSION=$(eval echo "$"$TEST_RES_IMAGE_OUT_UP"_VERSIONNUMBER")

export TEST_CURR_JOB_UP=$(echo $TEST_CURR_JOB | awk '{print toupper($0)}')
echo TEST_CURR_JOB=$JOB_NAME
echo TEST_CURR_JOB_UP=$TEST_CURR_JOB_UP

create_out_state() {
  echo "-----> Creating a state file for $TEST_RES_IMAGE_OUT"
  echo versionName="deploy.2" > "$JOB_STATE/$TEST_RES_IMAGE_OUT.env"
  #echo commitSHA=$TEST_REPO_COMMIT >> "$JOB_STATE/$TEST_RES_IMAGE_OUT.env" 
  cat "$JOB_STATE/$TEST_RES_IMAGE_OUT.env"
  
  echo "-----> Creating a state file for $TEST_CURR_JOB"
  echo versionName="deploy.2" > "$JOB_STATE/$TEST_CURR_JOB.env"
  cat "$JOB_STATE/$TEST_CURR_JOB.env"
  
  echo "-----> Creating a previous state file for $TEST_CURR_JOB"
  cat "$JOB_PREVIOUS_STATE/$TEST_CURR_JOB.env"  
}

main() {
  create_out_state    
}
main


echo "EOF!"
